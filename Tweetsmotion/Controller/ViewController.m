//
//  ViewController.m
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "ViewController.h"
#import "PrefixHeader.pch"

@interface ViewController ()
{
    TwitterSentimentModel *sentimentModel;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupLayout];
    
}


//MARK:- UIButton's action methods

- (IBAction)PredictTweetSentimentButtonTapped:(UIButton *)sender {

    if (![_tweetTextField.text isEqualToString: @""]) {
        [self searchTweetsWithText: _tweetTextField.text];
    } else {
        [self.view makeToast: @"Please enter tweet's hashtag or keyword."];
    }
    
}


//MARK:- User-defined methods

/*!
    @brief It sets up the initial layout for this controller.
*/
- (void) setupLayout {
    
    // Sets background color to a view.
    [self.view setBackgroundColor: [UIColor applicationBackgroundColor]];
    
    // Sets up a layout for title and subtitle label.
    [_appTitleLabel setTextColor: [UIColor whiteColor]];
    [self->_subtitleLabel setAlpha: 0.0];
    
    // Calls animateTitle method after 1 second of delay to animates the text of title label.
    [self performSelector: @selector(animateTitle) withObject: self afterDelay: 1.0];
    
    // Initializes the sentiment output label with the empty string.
    [_sentimentLabel setText: @""];
    
    // Sets up a layout of tweetTextField.
    [[_tweetTextField layer] setCornerRadius: 4.0];
    [_tweetTextField setBackgroundColor: [UIColor whiteColor]];
    
    // Sets up a layout of predictTweetSentiment button.
    [[_predictTweetSentimentButton layer] setMasksToBounds: NO];
    [[_predictTweetSentimentButton layer] setCornerRadius: (_predictTweetSentimentButton.frame.size.height / 2.0)];
    [_predictTweetSentimentButton setBackgroundColor: [UIColor whiteColor]];
    [_predictTweetSentimentButton setTitleColor: [UIColor applicationBackgroundColor] forState: UIControlStateNormal];
    [[_predictTweetSentimentButton layer] setShadowColor: [UIColor blackColor].CGColor];
    [[_predictTweetSentimentButton layer] setShadowOffset: CGSizeMake(0.0, 0.0)];
    [[_predictTweetSentimentButton layer] setShadowRadius: (_predictTweetSentimentButton.frame.size.height / 2.0)];
    [[_predictTweetSentimentButton layer] setShadowOpacity: 0.3];
    
    // Initializes the sentimentModel object.
    sentimentModel = [[TwitterSentimentModel alloc] init];
    
}

/*!
   @brief Animates the text of the appTitleLabel.
*/
- (void) animateTitle {
    
    // Initializes the appTitleLabel with empty string.
    [_appTitleLabel setText: @""];
    
    // Appends and displays string by looping through all characters of titleString with scheduled time interval.
    NSString *titleString = @"Tweetsmotion";
    for (int i = 0; i < titleString.length; i++) {
        [NSTimer scheduledTimerWithTimeInterval: (i * 0.1) repeats: NO block:^(NSTimer * _Nonnull timer) {
            [self->_appTitleLabel setText: [NSString stringWithFormat: @"%@%@", [self->_appTitleLabel text], [titleString substringWithRange: NSMakeRange(i, 1)]]];
        }];
    }
    
    // Animates subTitleLabel for duration of 1 second.
    [UIView animateWithDuration: 1.0 animations:^{
        [self->_subtitleLabel setAlpha: 1.0];
    }];
    
}

/*!
   @brief Collect tweets data based on the search query as an input for sentiment analysis of text parameter.

   @param text The tweet's hashtag or keyword string value.

   @code
       [self searchTweetsWithText: @"#Apple"];
   @endcode
*/
- (void) searchTweetsWithText: (NSString *) text {
    
    // Search tweets array based on the specified parameters.
    [[Helper STTwitterAPI] getSearchTweetsWithQuery: text geocode: @"" lang: kSearchTweetLanguage locale: @"" resultType: @"recent" count: kSearchTweetCount until: @"" sinceID: @"" maxID: @"" includeEntities: @0 callback: @"" useExtendedTweetMode: @1 successBlock:^(NSDictionary *searchMetadata, NSArray *statuses) {
        
        NSMutableArray *arrTweets = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < statuses.count; i++) {
            NSDictionary *dicTweet = [[NSDictionary alloc] initWithDictionary: [statuses objectAtIndex: i]];
            NSString *tweetString = [dicTweet valueForKey: @"full_text"];
            if (![[dicTweet valueForKey: @"full_text"] isEqualToString: @""]) {
                
                // Creates model input containing tweet string for analysis and stores it in an array.
                TwitterSentimentModelInput *input = [[TwitterSentimentModelInput alloc] initWithText: tweetString];
                [arrTweets addObject: input];
            }
        }
        
        [self predictSentimentForTweets: arrTweets];
        
    } errorBlock:^(NSError *error) {
        [self.view makeToast: @"Something went wrong. Please try after few minutes!"];
    }];
    
}

/*!
   @brief Predicts sentiment of a tweet from tweets array returned from Twitter API.

   @param tweets Array containing tweets.

   @code
       [self predictSentimentForTweets: arrTweets];
   @endcode
*/
- (void) predictSentimentForTweets: (NSMutableArray *) tweets {
    
    // Initialise sentimentScore with 0
    NSInteger *sentimentScore = 0;
    
    // Initialise MLPredictionOptions object with the UsesCPUOnly property to true.
    MLPredictionOptions *options = [[MLPredictionOptions alloc] init];
    [options setUsesCPUOnly: YES];
    
    // Returns an array as an output containing label of a tweet string.
    NSError *error;
    NSArray *outputs = [[NSArray alloc] initWithArray: [sentimentModel predictionsFromInputs: tweets options: options error: &error]];
    
    // Checks for positive and negetive label, and increments or decrements the sentimentScore accordingly.
    if (!error) {
        for (TwitterSentimentModelOutput *output in outputs) {
            NSString *sentiment = output.label;
            if ([sentiment isEqualToString: @"Pos"]) {
                sentimentScore += 1;
            } else if ([sentiment isEqualToString: @"Neg"]) {
                sentimentScore -= 1;
            }
        }
        [self UpdateSentimentLabelFromSentimentScore: sentimentScore];
    } else {
        [self.view makeToast: @"Something went wrong. Please try after few minutes!"];
    }
    
}

/*!
   @brief Updates sentiment label according to the sentiment score passed as an parameter.

   @param score Sentiment score counts.

   @code
       [self UpdateSentimentLabelFromSentimentScore: 17];
   @endcode
*/
- (void) UpdateSentimentLabelFromSentimentScore: (NSInteger *) score {
    
    int sentimentScore = (int) score;
    if (sentimentScore > 20) {
        [_sentimentLabel setText: @"😍"];
    } else if (sentimentScore > 10) {
        [_sentimentLabel setText: @"😀"];
    } else if (sentimentScore > 0) {
        [_sentimentLabel setText: @"🙂"];
    } else if (sentimentScore == 0) {
        [_sentimentLabel setText: @"😐"];
    } else if (sentimentScore > -10) {
        [_sentimentLabel setText: @"☹️"];
    } else if (sentimentScore > -20) {
        [_sentimentLabel setText: @"😡"];
    } else {
        [_sentimentLabel setText: @"🤮"];
    }
    
}


@end
