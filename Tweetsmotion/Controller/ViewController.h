//
//  ViewController.h
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>


/*!
 
    @header ViewController
 
    @brief The ViewController class.

    @discussion This class implements the functionalities of analyzing the sentiment of searched tweet with the help of TwitterSentimentModel and displaying its sentiment in the form of emoji.

*/
@interface ViewController : UIViewController

/*! A label for displaying the slogan of this app.  */
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

/*! A label for displaying app name. */
@property (weak, nonatomic) IBOutlet UILabel *appTitleLabel;

/*! A label for displaying emoji as an output of searched tweet. */
@property (weak, nonatomic) IBOutlet UILabel *sentimentLabel;

/*! A textField for receiving user input of tweet's hashtag or keyword for analyzing its sentiment. */
@property (weak, nonatomic) IBOutlet UITextField *tweetTextField;

/*! A label for describing the uses of the tweetTextField object. */
@property (weak, nonatomic) IBOutlet UILabel *discriptionLabel;

/*! A button's outlet object that triggers the event of analyzing and displaying the sentiment of entered tweet hashtag or keyword. */
@property (weak, nonatomic) IBOutlet UIButton *predictTweetSentimentButton;

/*! A button's action that triggers the event of analyzing and displaying the sentiment of entered tweet hashtag or keyword. */
- (IBAction)PredictTweetSentimentButtonTapped:(UIButton *)sender;

@end

