//
//  Helper.h
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TweetsmotionKeys.h>
#import <STTwitter/STTwitter.h>

NS_ASSUME_NONNULL_BEGIN

/*!
   @header Helper

   @brief A class for common methods used in the project.

   @discussion This class contain common methods that are useful throughout the project.

   @code
       STTwitterAPI *twitterAPI = [Helper STTwitterAPI];
   @endcode
*/
@interface Helper : NSObject

/*!
    @brief It creates, initializes and returns an object of TweetsmotionKeys class.
 
    @return TweetsmotionKeys: A TweetsmotionKeys's object.

    @code
        NSString *twitterConsumerKey = [self TweetsmotionKeys].twitterConsumerKey;
    @endcode
*/
+ (TweetsmotionKeys *) TweetsmotionKeys;

/*!
    @brief It returns an object of STTwitterAPI class initialized with four parameters: consumerKey, consumerSecret, oauthToken and oauthTokenSecret, which is used for working with properties and methods defined in this class.
 
    @return STTwitterAPI: A STTwitterAPI's object.

    @code
        NSString *username = [[Helper STTwitterAPI] userName];
    @endcode
*/
+ (STTwitterAPI *) STTwitterAPI;

@end

NS_ASSUME_NONNULL_END
