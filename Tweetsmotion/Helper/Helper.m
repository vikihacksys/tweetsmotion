//
//  Helper.m
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "Helper.h"
#import "PrefixHeader.pch"

@implementation Helper

+ (TweetsmotionKeys *) TweetsmotionKeys {
    TweetsmotionKeys *keys = [[TweetsmotionKeys alloc] init];
    return keys;
}

+ (STTwitterAPI *) STTwitterAPI {
    STTwitterAPI *twitterAPI = [STTwitterAPI
                                twitterAPIWithOAuthConsumerKey: [self TweetsmotionKeys].twitterConsumerKey consumerSecret: [self TweetsmotionKeys].twitterConsumerSecret
                                    oauthToken: [self TweetsmotionKeys].twitterOAuthToken
                                    oauthTokenSecret: [self TweetsmotionKeys].twitterOAuthTokenSecret];
    return twitterAPI;
}

@end
