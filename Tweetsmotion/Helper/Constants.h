//
//  Constants.h
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//


/*!
   @header Constants

   @brief An Objective-C header file.

   @discussion This file defines all constants that are used globally in the project. It can be accessible directly from any other classes defined in the same project scoope.
*/

#ifndef Constants_h
#define Constants_h


#endif /* Constants_h */

/*! Defines language constant for a tweet search query.  */
static NSString *kSearchTweetLanguage = @"en";

/*! Defines maximum count for returing tweets for a search criteria.  */
static NSString *kSearchTweetCount = @"100";

