//
//  UIColor+Style.h
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/*!
    @header UIColor+Style
 
    @brief A category of UIColor class.
 
    @discussion This category uses UIColor class to add custom color styles to the UIColor class.
 
    @code
        [self.view setBackgroundColor: [UIColor applicationBackgroundColor]];
    @endcode
 */
@interface UIColor (Style)

/*!
    @brief It returns the UIColor object having custom color values of Red, Green, Blue (27, 162, 239) for application background colour.
 
    @return UIColor: An UIColor object, intialized with custom color values.

    @code
        [self.view setBackgroundColor: [UIColor applicationBackgroundColor]];
    @endcode
*/
+ (UIColor *) applicationBackgroundColor;

@end

NS_ASSUME_NONNULL_END
