//
//  UIColor+Style.m
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import "UIColor+Style.h"

@implementation UIColor (Style)

+ (UIColor *) applicationBackgroundColor {
    return [UIColor colorWithRed: 27.0/255.0 green: 162.0/255.0 blue: 239.0/255.0 alpha: 1.0];
}

@end
