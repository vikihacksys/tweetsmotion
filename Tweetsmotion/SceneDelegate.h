//
//  SceneDelegate.h
//  Tweetsmotion
//
//  Created by Vivek Radadiya on 02/09/20.
//  Copyright © 2020 Vivek Radadiya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

