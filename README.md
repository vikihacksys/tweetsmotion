
# Tweetsmotion

> An iOS application that allow users to search for emotions of tweets from Twitter data.

## Screenshots
![Screen 1](Tweetsmotion/Microsoft.png)
![Screen 2](Tweetsmotion/Google.png)
![Screen 3](Tweetsmotion/Corona.png)

## Technologies
* Objective - C
* CoreML

## Features
* MVC Design Pattern
* Twitter Sentiment Analysis

## Contact
Created by [@vikihacksys](https://gitlab.com/vikihacksys) - feel free to contact me!
